package com.carlisty.carlisty.webservices.vehicles

import com.carlisty.carlisty.models.Dealer
import com.carlisty.carlisty.models.Vehicle
import com.carlisty.carlisty.webservices.BaseResponse
import com.google.gson.annotations.SerializedName

/**
 * Created by Raphzz on 12/04/2018.
 */

class VehicleResponse : BaseResponse() {
    @SerializedName("content")
    var vehicles: ArrayList<Vehicle> = ArrayList()
        internal set
}