package com.carlisty.carlisty.webservices

import com.google.gson.annotations.SerializedName

/**
 * Created by Raphzz on 11/04/2018.
 */

open class BaseResponse {
    @SerializedName("content")
    var content: List<Object>? = null
        internal set
}