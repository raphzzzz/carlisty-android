package com.carlisty.carlisty.webservices.vehicles

import com.carlisty.carlisty.Globals
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ListVehiclesRequest : Serializable {
    @SerializedName("token")
    var token: String? = Globals.currentUser!!.token
        internal set

    @SerializedName("dealerID")
    var dealerID: String? = null
        internal set
}
