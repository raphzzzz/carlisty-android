package com.carlisty.carlisty.webservices.vehicles

import com.carlisty.carlisty.webservices.BaseResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Created by Raphzz on 11/04/2018.
 */

interface VehiclesAPI {

    @Headers("Accept: application/json", "Content-Type: application/json; charset=utf-8", "Accept-Language: en")
    @POST("/findVehicles")
    fun getVehicles(@Body request: ListVehiclesRequest): Call<BaseResponse>
}