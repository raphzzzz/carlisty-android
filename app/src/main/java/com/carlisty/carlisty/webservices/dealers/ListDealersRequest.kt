package com.carlisty.carlisty.webservices.dealers

import com.carlisty.carlisty.Globals
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ListDealersRequest : Serializable {
    @SerializedName("token")
    var token: String? = Globals.currentUser!!.token
        internal set
}
