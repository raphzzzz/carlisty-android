package com.carlisty.carlisty.webservices.dealers

import com.carlisty.carlisty.webservices.BaseResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

/**
 * Created by Raphzz on 11/04/2018.
 */

interface DealersAPI {

    @Headers("Accept: application/json", "Content-Type: application/json; charset=utf-8", "Accept-Language: en")
    @POST("/listDealers")
    fun getDealers(@Body request: ListDealersRequest): Call<BaseResponse>
}