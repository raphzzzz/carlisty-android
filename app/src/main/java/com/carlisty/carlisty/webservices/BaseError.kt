package com.carlisty.carlisty.webservices

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Raphzz on 12/04/2018.
 */
class BaseError : Serializable {

    @SerializedName("message")
    var msg: String? = "Failed to reach server"
        internal set
}