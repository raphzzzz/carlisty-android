package com.carlisty.carlisty.webservices.login

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class LoginRequest(email: String?, pass: String?) : Serializable {
    @SerializedName("email")
    var email: String? = email
        internal set
    @SerializedName("password")
    var password: String? = pass
        internal set
}
