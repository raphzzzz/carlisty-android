package com.carlisty.carlisty.webservices.dealers

import com.carlisty.carlisty.models.Dealer
import com.carlisty.carlisty.webservices.BaseResponse
import com.google.gson.annotations.SerializedName

/**
 * Created by Raphzz on 12/04/2018.
 */

class DealerResponse : BaseResponse() {
    @SerializedName("content")
    var dealers: ArrayList<Dealer> = ArrayList()
        internal set
}