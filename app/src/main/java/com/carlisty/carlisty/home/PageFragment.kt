package com.carlisty.carlisty.home

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.carlisty.carlisty.R


/**
 * Created by Raphzz on 11/04/2018.
 */
class PageFragment : Fragment() {

    var index = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return when(index) {
            0 -> inflater!!.inflate(R.layout.home_view_pager_page_1, container, false)
            1 -> inflater!!.inflate(R.layout.home_view_pager_page_2, container, false)
            2 -> inflater!!.inflate(R.layout.home_view_pager_page_3, container, false)
            3 -> inflater!!.inflate(R.layout.home_view_pager_page_4, container, false)
            else -> inflater!!.inflate(R.layout.home_view_pager_page_1, container, false)
        }
    }

    companion object {
        fun newInstance(index: Int): PageFragment {
            val fragment = PageFragment()
            fragment.index = index

            return fragment
        }
    }
}