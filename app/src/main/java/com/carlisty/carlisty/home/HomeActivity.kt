package com.carlisty.carlisty.home

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.carlisty.carlisty.R
import kotlinx.android.synthetic.main.activity_home.*
import android.content.Intent
import com.carlisty.carlisty.login.LoginActivity


class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        home_viewpager.adapter = HomeViewPagerAdapter(supportFragmentManager)

        pageIndicatorView.count = 4
        pageIndicatorView.selection = 0

        setupUI()
    }

    private fun setupUI() {
        home_login_button.setOnClickListener {
            startActivity(Intent(this, LoginActivity::class.java))
        }
    }
}
