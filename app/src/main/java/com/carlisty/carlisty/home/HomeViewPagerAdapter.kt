package com.carlisty.carlisty.home

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter



/**
 * Created by Raphzz on 11/04/2018.
 */
class HomeViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(pos: Int): Fragment {
        return when (pos) {
            0 -> PageFragment.newInstance(0)
            1 -> PageFragment.newInstance(1)
            2 -> PageFragment.newInstance(2)
            3 -> PageFragment.newInstance(3)
            else -> PageFragment.newInstance(0)
        }
    }

    override fun getCount(): Int {
        return 4
    }
}