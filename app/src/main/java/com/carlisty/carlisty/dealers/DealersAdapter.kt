package com.carlisty.carlisty.dealers

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.carlisty.carlisty.R
import com.carlisty.carlisty.base.BaseRecyclerAdapter
import com.carlisty.carlisty.base.LineItem
import com.carlisty.carlisty.models.Dealer
import java.util.ArrayList

/**
 * Created by Raphzz on 13/04/2018.
 */

class DealersAdapter(private val mContext: Context, mListItems: List<LineItem>, private val mListener: OnDealerClickListener) : BaseRecyclerAdapter() {

    private var mListItems: List<LineItem> = ArrayList()

    init {
        this.mListItems = mListItems
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.row_dealer, parent, false)
        return DealerHolder(mContext, view, mListener)
    }

    override fun setList(list: List<LineItem>) {
        super.setList(list)
        this.mListItems = list
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = mListItems[position]
        (holder as DealerHolder).bindItem(position, item)
    }

    override fun getItemCount(): Int {
        return mListItems.size
    }

    interface OnDealerClickListener {
        fun onDealerSelected(dealer: Dealer)
    }
}