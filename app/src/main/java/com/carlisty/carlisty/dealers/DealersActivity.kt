package com.carlisty.carlisty.dealers

import android.os.Bundle
import com.carlisty.carlisty.R
import com.carlisty.carlisty.base.BaseNavigationActivity

class DealersActivity : BaseNavigationActivity() {

    private var presenter: DealersPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dealers)

        presenter = DealersPresenter(this)

        presenter!!.loadDealers()
    }
}
