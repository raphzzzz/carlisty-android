package com.carlisty.carlisty.dealers

import com.carlisty.carlisty.Globals
import com.carlisty.carlisty.base.BaseInteractor
import com.carlisty.carlisty.models.Dealer
import com.carlisty.carlisty.utils.Constants
import com.carlisty.carlisty.webservices.BaseResponse
import com.carlisty.carlisty.webservices.dealers.DealerResponse
import com.carlisty.carlisty.webservices.dealers.DealersAPI
import com.carlisty.carlisty.webservices.dealers.ListDealersRequest
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Raphzz on 12/04/2018.
 */

class DealersInteractor : BaseInteractor() {

    fun getDealers(listener: DealersListener) {
        val api = createAPI(Constants().DOMAIN_URL, DealersAPI::class.java) as DealersAPI

        val call = api.getDealers(ListDealersRequest())

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                if (!response.isSuccessful && response.code() != 200) {
                    listener.onDealersError()
                    return
                }

                val baseResponse = response.body()
                val dealerResponse = DealerResponse()

                for(content in baseResponse.content!!) {
                    val gson = Gson()
                    val dealer = gson.fromJson(gson.toJson(content), Dealer::class.java)
                    dealerResponse.dealers.add(dealer)
                }

                listener.onDealersLoaded(dealerResponse)
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                listener.onDealersError()
            }
        })
    }
}