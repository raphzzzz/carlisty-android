package com.carlisty.carlisty.dealers

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.carlisty.carlisty.R
import com.carlisty.carlisty.base.LineItem
import com.carlisty.carlisty.models.Dealer
import com.squareup.picasso.Picasso
import com.squareup.picasso.PicassoProvider

/**
 * Created by Raphzz on 13/04/2018.
 */

class DealerHolder(internal var mContext: Context, itemView: View, private val mListener: DealersAdapter.OnDealerClickListener) : RecyclerView.ViewHolder(itemView) {

    private val mRowContainer: LinearLayout = itemView.findViewById(R.id.row_container)
    private var mDealerNameTv: TextView = itemView.findViewById(R.id.row_dealer_name)
    private var mDealerAddressTv: TextView = itemView.findViewById(R.id.row_dealer_address)
    private var mDealerProfileImage: ImageView = itemView.findViewById(R.id.row_dealer_profile_image)

    fun bindItem(position: Int, item: LineItem) {
        val dealerObj = item.obj as Dealer

        mDealerNameTv.text = dealerObj.name
        mDealerAddressTv.text = dealerObj.address

        if (dealerObj.profileImage != null) {
            Picasso.get().load(dealerObj.profileImage).into(mDealerProfileImage)
        }

        mRowContainer.setOnClickListener { mListener.onDealerSelected(dealerObj) }
    }
}