package com.carlisty.carlisty.dealers

import android.content.Intent
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.carlisty.carlisty.base.LineItem
import com.carlisty.carlisty.models.Dealer
import com.carlisty.carlisty.webservices.dealers.DealerResponse
import kotlinx.android.synthetic.main.activity_dealers.*
import java.util.ArrayList
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import com.carlisty.carlisty.R
import com.carlisty.carlisty.vehicles.VehiclesActivity


/**
 * Created by Raphzz on 12/04/2018.
 */

class DealersPresenter(private val dealersActivity: DealersActivity) : DealersListener, DealersAdapter.OnDealerClickListener {

    private val dealersInteractor = DealersInteractor()

    fun loadDealers() {
        dealersInteractor.getDealers(this)
        dealersActivity.hideApiErrorScreen()
        dealersActivity.showProgress()
    }

    override fun onDealersError() {
        dealersActivity.stopProgress()
        dealersActivity.showApiErrorScreen({
            loadDealers()
        })
    }

    override fun onDealersLoaded(dealerResponse: DealerResponse) {
        dealersActivity.stopProgress()
        setupRecyclerView(dealerResponse.dealers)
    }

    override fun onDealerSelected(dealer: Dealer) {
        val i = Intent(dealersActivity, VehiclesActivity::class.java)
        i.putExtra("dealerID", dealer.id)
        dealersActivity.startActivity(i)
    }

    private fun setupRecyclerView(dealers: List<Dealer>) {
        dealersActivity.recyclerView.setHasFixedSize(false)
        dealersActivity.recyclerView.itemAnimator = DefaultItemAnimator()
        dealersActivity.recyclerView.layoutManager = LinearLayoutManager(dealersActivity)

        val itemDecorator = DividerItemDecoration(dealersActivity, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(dealersActivity, R.drawable.divider)!!)

        dealersActivity.recyclerView.addItemDecoration(itemDecorator)

        val itemsList = ArrayList<LineItem>()

        for (dealer in dealers) {
            itemsList.add(LineItem(dealer , false, 0, 0))
        }

        val adapter = DealersAdapter(dealersActivity, itemsList, this)
        dealersActivity.recyclerView.adapter = adapter
    }
}