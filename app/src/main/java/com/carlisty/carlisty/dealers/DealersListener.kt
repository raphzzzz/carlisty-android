package com.carlisty.carlisty.dealers

import com.carlisty.carlisty.webservices.dealers.DealerResponse

/**
 * Created by Raphzz on 12/04/2018.
 */

interface DealersListener {
    fun onDealersLoaded(dealerResponse: DealerResponse)
    fun onDealersError()
}