package com.carlisty.carlisty.widget

/**
 * Created by Raphzz on 11/04/2018.
 */
import android.content.Context
import android.support.v7.widget.AppCompatEditText
import android.util.AttributeSet
import com.carlisty.carlisty.R
import com.carlisty.carlisty.utils.FontCache

/**
 * Created by raphaelpedrinivelasqua on 18/05/17.
 */

class CarlistyEditText : AppCompatEditText {

    private var mContext: Context

    constructor(context: Context) : super(context) {
        mContext = context
        setCustomFont()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        mContext = context
        setCustomFont()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mContext = context
        setCustomFont()
    }

    private fun setCustomFont() {
//        val tf = FontCache[mContext.getString(R.string.raleway_custom_font_path), mContext]
//        if (tf != null) {
//            typeface = tf
//        }
    }
}