package com.carlisty.carlisty.details

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.carlisty.carlisty.R
import com.carlisty.carlisty.base.BaseNavigationActivity
import com.carlisty.carlisty.models.Vehicle

class VehicleDetailsActivity : BaseNavigationActivity() {

    private lateinit var presenter: VehicleDetailsPresenter

    private lateinit var vehicleSelected: Vehicle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        presenter = VehicleDetailsPresenter(this)

        val intent = this.intent
        val bundle = intent.extras

        vehicleSelected = bundle.getSerializable("vehicle") as Vehicle

        presenter.attach360View(vehicleSelected)

        presenter.playVideoUrl(vehicleSelected)

        presenter.populateImages(vehicleSelected)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_activity_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
        // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                onBackPressed()
                return true
            }

            R.id.action_share -> {
                openSmsIntent()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}
