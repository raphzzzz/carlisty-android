package com.carlisty.carlisty.details

import android.graphics.Color
import android.net.Uri
import android.widget.ImageView
import android.widget.LinearLayout
import com.carlisty.carlisty.models.Vehicle
import com.carlisty.carlisty.utils.Constants
import kotlinx.android.synthetic.main.activity_details.*
import android.widget.MediaController
import android.opengl.ETC1.getHeight
import android.view.ViewTreeObserver
import com.squareup.picasso.Picasso


/**
 * Created by Raphzz on 16/04/2018.
 */

class VehicleDetailsPresenter(private val vehicleDetailsActivity: VehicleDetailsActivity) {

    fun attach360View(vehicle: Vehicle) {

        vehicleDetailsActivity.webView.setInitialScale(1)
        vehicleDetailsActivity.webView.settings.javaScriptEnabled = true
        vehicleDetailsActivity.webView.settings.loadWithOverviewMode = true
        vehicleDetailsActivity.webView.settings.useWideViewPort = true

        val url = String.format(Constants().SIRV_DOMAIN_URL, vehicle.views.toString(), vehicle.sirvId) + Constants().SIRV_360_SPIN_POSFIX

        vehicleDetailsActivity.webView.loadUrl(url)
    }

    fun playVideoUrl(vehicle: Vehicle) {
        val url = String.format(Constants().SIRV_MEDIA_URL, vehicle.views.toString(), vehicle.sirvId)

        val mc = MediaController(vehicleDetailsActivity)
        mc.setAnchorView(vehicleDetailsActivity.videoView)
        mc.setMediaPlayer(vehicleDetailsActivity.videoView)
        val video = Uri.parse(url)
        vehicleDetailsActivity.videoView.setMediaController(mc)
        vehicleDetailsActivity.videoView.setVideoURI(video)
        vehicleDetailsActivity.videoView.start()
    }

    fun populateImages(vehicle: Vehicle) {

        vehicleDetailsActivity.scrollView_images.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                vehicleDetailsActivity.scrollView_images.viewTreeObserver.removeOnGlobalLayoutListener(this)
                vehicleDetailsActivity.scrollView_images.height //height is ready

                val imagesIndexes = arrayOf("001", "063", "051", "045", "038", "026", "014", "008")

                for (index in imagesIndexes) {
                    val params = LinearLayout.LayoutParams(vehicleDetailsActivity.scrollView_images.height, vehicleDetailsActivity.scrollView_images.height)
                    params.marginStart = 50

                    val imageView = ImageView(vehicleDetailsActivity)
                    imageView.scaleType = ImageView.ScaleType.FIT_CENTER
                    imageView.layoutParams = params

                    Picasso.get().load(String.format(Constants().SIRV_DOMAIN_IMAGE_URL, vehicle.views, vehicle.sirvId, index)).into(imageView)

                    vehicleDetailsActivity.layout_images.addView(imageView)
                }
            }
        })
    }
}