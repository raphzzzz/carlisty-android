package com.carlisty.carlisty.base

import android.app.ActionBar
import android.graphics.Color
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.base_loading_layout.*
import com.carlisty.carlisty.R
import kotlinx.android.synthetic.main.view_api_error.*
import android.opengl.ETC1.getHeight
import android.opengl.ETC1.getWidth
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.ViewTreeObserver
import android.util.TypedValue
import android.content.res.TypedArray
import android.opengl.ETC1.getHeight
import android.view.Display
import android.R.attr.y
import android.graphics.Point
import android.os.Build
import android.view.WindowManager
import kotlinx.android.synthetic.main.activity_dealers.*
import android.content.Intent




open class BaseNavigationActivity : AppCompatActivity() {

    private var apiErrorView: View? = null

    private var loadingActivityView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (supportActionBar != null) {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        }
    }

    fun setTitle(context: AppCompatActivity, title: String) {
        if (supportActionBar != null) {
            context.supportActionBar!!.title = title
        }
    }

    fun showProgress() {
        val viewGroup = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup

        if (loadingActivityView != null) {
            viewGroup.addView(loadingActivityView)
            avi.smoothToShow()
            return
        }

        loadingActivityView = layoutInflater.inflate(R.layout.base_loading_layout, null)

        viewGroup.addView(loadingActivityView)
        avi.smoothToShow()
    }

    fun stopProgress() {
        val viewGroup = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup
        viewGroup.removeView(loadingActivityView)
    }

    fun showApiErrorScreen(callback: () -> Unit) {
        val viewGroup = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup

        if (apiErrorView != null) {
            viewGroup.addView(apiErrorView)
            return
        }

        apiErrorView = layoutInflater.inflate(R.layout.view_api_error, null)

        viewGroup.addView(apiErrorView)

        retry_api_request.setOnClickListener {
            callback()
        }
    }

    fun hideApiErrorScreen() {
        val viewGroup = (this.findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup
        viewGroup.removeView(apiErrorView)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
        // Respond to the action bar's Up/Home button
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getDisplayContentHeight(): Int {
        val windowManager = windowManager
        val size = Point()
        var screenHeight: Int
        var actionBarHeight = 0
        if (actionBar != null) {
            actionBarHeight = actionBar!!.height
        }
        val contentTop = (findViewById<View>(android.R.id.content) as ViewGroup).top
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            windowManager.defaultDisplay.getSize(size)
            screenHeight = size.y
        } else {
            val d = windowManager.defaultDisplay
            screenHeight = d.height
        }
        return screenHeight - contentTop - actionBarHeight
    }

    fun openSmsIntent(message: String = "Testing :)") {
        val smsIntent = Intent(Intent.ACTION_VIEW)
        smsIntent.type = "vnd.android-dir/mms-sms"
        smsIntent.putExtra("sms_body", message)
        startActivity(smsIntent)
    }
}
