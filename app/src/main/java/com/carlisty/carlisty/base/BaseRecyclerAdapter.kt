package com.carlisty.carlisty.base

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.carlisty.carlisty.R

/**
 * Created by Raphzz on 13/04/2018.
 */

open class BaseRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.row_dealer, parent, false)
        return BaseHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

    }

    open fun setList(list: List<LineItem>) {}

    open fun filterList(text: String) {}

    override fun getItemCount(): Int {
        return 0
    }
}