package com.carlisty.carlisty.base

interface BaseView {

    fun showLoading()
    fun hideLoading()
}