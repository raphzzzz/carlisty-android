package com.carlisty.carlisty.base

import java.util.*

/**
 * Created by Raphzz on 13/04/2018.
 */
class LineItem(obj: Any, isHeader: Boolean, sectionFirstPosition: Int, type: Int) {

    var sectionFirstPosition = 0
    var type = 0
    var isHeader = false
    var obj: Any? = null
    var searchText: String? = null

    init {
        this.sectionFirstPosition = sectionFirstPosition
        this.type = type
        this.isHeader = isHeader
        this.obj = obj
    }
}