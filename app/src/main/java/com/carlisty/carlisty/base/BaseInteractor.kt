package com.carlisty.carlisty.base

import android.content.Context
import com.carlisty.carlisty.webservices.BaseError
import com.carlisty.carlisty.webservices.Deserializer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonArray
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

/**
 * Created by Raphzz on 12/04/2018.
 */

open class BaseInteractor {
    private var retrofit: Retrofit? = null

    fun createAPI(domain: String, apiClass: Class<*>): Any {
        val gson = GsonBuilder().registerTypeAdapter(Deserializer::class.java, Deserializer()).create()

        retrofit = Retrofit.Builder().baseUrl(domain)
                .addConverterFactory(GsonConverterFactory.create(gson)).build()

//                retrofit.client().setConnectTimeout(60, TimeUnit.SECONDS);
//                retrofit.client().setWriteTimeout(60, TimeUnit.SECONDS);

        return retrofit!!.create(apiClass)
    }

    fun createEmptyResponseAPI(domain: String, apiClass: Class<*>): Any {
        val gson = GsonBuilder().registerTypeAdapter(Deserializer::class.java, Deserializer()).create()

        retrofit = Retrofit.Builder().baseUrl(domain)
                .addConverterFactory(GsonConverterFactory.create(gson)).build()

        //        retrofit.client().setConnectTimeout(60, TimeUnit.SECONDS);
        //        retrofit.client().setWriteTimeout(60, TimeUnit.SECONDS);

        return retrofit!!.create(apiClass)
    }

    fun buildErrorMessageFromArray(messages: Array<String>?): String {

        if (messages == null)
            return ""

        val finalMessage = StringBuilder()
        var i = 0
        for (message in messages) {
            finalMessage.append(message)

            if (i < messages.size - 1)
                finalMessage.append("\n")

            i++
        }

        return finalMessage.toString()
    }

    fun getError(response: Response<*>?): BaseError? {

        if (response != null && !response.isSuccessful && response.errorBody() != null) {

            val errorConverter = retrofit!!.responseBodyConverter<Any>(BaseError::class.java!!, arrayOfNulls<Annotation>(0))
            try {
                return errorConverter.convert(response.errorBody()) as BaseError
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        }

        return null
    }

    fun convetJsonToObjct(array: JsonArray, clazz: Class<*>, gson: Gson): List<Any> {
        val listT = ArrayList<Any>()
        (0..array.size() - 1).mapTo(listT) { gson.fromJson(array.get(it), clazz) }
        return listT
    }

    companion object {

        val TAG = "BaseInteractorImpl"
        val CONTENT_TYPE_MULTPART = "application/json"

        val HEADER_TOKEN_INDEX = 0
        val HEADER_CLIENT_INDEX = 1
        val HEADER_UID_INDEX = 2

        val SERVER_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss"
        val SERVER_DATE_PATTERN_TIME_ZONE = "yyyy-MM-dd'T'HH:mm:ss.sss"

        protected fun doRequest() {}
    }
}