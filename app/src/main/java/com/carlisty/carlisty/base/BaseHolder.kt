package com.carlisty.carlisty.base

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.carlisty.carlisty.R
import com.carlisty.carlisty.base.LineItem
import com.carlisty.carlisty.models.Dealer
import com.carlisty.carlisty.utils.ImageUtils

/**
 * Created by Raphzz on 13/04/2018.
 */

class BaseHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bindItem(position: Int, item: LineItem) {

    }
}