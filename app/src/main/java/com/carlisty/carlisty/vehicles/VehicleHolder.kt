package com.carlisty.carlisty.vehicles

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import com.carlisty.carlisty.R
import com.carlisty.carlisty.base.LineItem
import com.carlisty.carlisty.models.Vehicle
import com.carlisty.carlisty.utils.Constants
import com.squareup.picasso.Picasso

/**
 * Created by Raphzz on 13/04/2018.
 */

class VehicleHolder(internal var mContext: Context, itemView: View, private val mListener: VehiclesAdapter.OnVehiclesClickListener) : RecyclerView.ViewHolder(itemView) {

    private var mRowContainer: RelativeLayout = itemView.findViewById(R.id.row_container)

    private var mVehicleThumbImage: ImageView = itemView.findViewById(R.id.row_vehicle_thumb_image)
    private var mVehicleModelTv: TextView = itemView.findViewById(R.id.row_vehicle_model)
    private var mVehicleDealerIdTv: TextView = itemView.findViewById(R.id.row_vehicle_dealerId)
    private var mVehicleExteriorColorTv: TextView = itemView.findViewById(R.id.row_vehicle_exteriorColor)

    fun bindItem(position: Int, item: LineItem) {
        val vehicleObj = item.obj as Vehicle

        mVehicleModelTv.text = vehicleObj.model
        mVehicleDealerIdTv.text = vehicleObj.dealerID
        mVehicleExteriorColorTv.text = vehicleObj.exteriorColor

        // GET THUMBNAIL
        Picasso.get().load(String.format(Constants().SIRV_DOMAIN_IMAGE_URL, vehicleObj.views.toString(), vehicleObj.sirvId, "001")).into(mVehicleThumbImage)

        mRowContainer.setOnClickListener {
            mListener.vehicleSelected(vehicleObj)
        }
    }
}