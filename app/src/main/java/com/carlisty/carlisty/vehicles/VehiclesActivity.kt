package com.carlisty.carlisty.vehicles

import android.os.Bundle
import com.carlisty.carlisty.R
import com.carlisty.carlisty.base.BaseNavigationActivity

class VehiclesActivity : BaseNavigationActivity() {

    private var presenter: VehiclesPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vehicles)

        presenter = VehiclesPresenter(this)

        // READ DEALER ID
        retrieveDealerIdFromBundle()

        presenter!!.loadVehicles()
    }

    private fun retrieveDealerIdFromBundle() {
        val dealerID = intent.extras.getString("dealerID")
        presenter!!.dealerSelectedID = dealerID
    }
}
