package com.carlisty.carlisty.vehicles

import com.carlisty.carlisty.webservices.vehicles.VehicleResponse

/**
 * Created by Raphzz on 12/04/2018.
 */

interface VehiclesListener {
    fun onVehiclesLoaded(vehiclesResponse: VehicleResponse)
    fun onVehiclesError()
}