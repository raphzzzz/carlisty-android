package com.carlisty.carlisty.vehicles

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import com.carlisty.carlisty.base.LineItem
import java.util.ArrayList
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import com.carlisty.carlisty.R
import com.carlisty.carlisty.details.VehicleDetailsActivity
import com.carlisty.carlisty.models.Vehicle
import com.carlisty.carlisty.utils.Constants
import com.carlisty.carlisty.webservices.vehicles.ListVehiclesRequest
import com.carlisty.carlisty.webservices.vehicles.VehicleResponse
import kotlinx.android.synthetic.main.activity_vehicles.*


/**
 * Created by Raphzz on 12/04/2018.
 */

class VehiclesPresenter(private val vehiclesActivity: VehiclesActivity) : VehiclesListener, VehiclesAdapter.OnVehiclesClickListener {
    private val vehiclesInteractor = VehiclesInteractor()

    var dealerSelectedID: String? = null

    fun loadVehicles() {

        val request = ListVehiclesRequest()

        request.dealerID = dealerSelectedID

        vehiclesInteractor.getVehicles(this, request)
        vehiclesActivity.showProgress()
    }

    override fun onVehiclesLoaded(vehiclesResponse: VehicleResponse) {
        vehiclesActivity.stopProgress()
        setupRecyclerView(vehiclesResponse.vehicles)
    }

    override fun onVehiclesError() {
        vehiclesActivity.stopProgress()
        vehiclesActivity.showApiErrorScreen {
            loadVehicles()
        }
    }

    override fun vehicleSelected(vehicle: Vehicle) {
        val bundle = Bundle()
        bundle.putSerializable("vehicle", vehicle)

        val intent = Intent(vehiclesActivity, VehicleDetailsActivity::class.java)
        intent.putExtras(bundle)
        vehiclesActivity.startActivity(intent)
    }

    private fun setupRecyclerView(vehicles: List<Vehicle>) {
        vehiclesActivity.recyclerView.setHasFixedSize(false)
        vehiclesActivity.recyclerView.itemAnimator = DefaultItemAnimator()
        vehiclesActivity.recyclerView.layoutManager = LinearLayoutManager(vehiclesActivity)

        val itemDecorator = DividerItemDecoration(vehiclesActivity, DividerItemDecoration.VERTICAL)
        itemDecorator.setDrawable(ContextCompat.getDrawable(vehiclesActivity, R.drawable.divider)!!)

        vehiclesActivity.recyclerView.addItemDecoration(itemDecorator)

        val itemsList = ArrayList<LineItem>()

        for (vehicle in vehicles) {
            itemsList.add(LineItem(vehicle , false, 0, 0))
        }

        val adapter = VehiclesAdapter(vehiclesActivity, itemsList, this)
        vehiclesActivity.recyclerView.adapter = adapter
    }
}