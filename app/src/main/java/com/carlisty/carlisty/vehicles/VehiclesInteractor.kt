package com.carlisty.carlisty.vehicles

import com.carlisty.carlisty.base.BaseInteractor
import com.carlisty.carlisty.models.Dealer
import com.carlisty.carlisty.models.Vehicle
import com.carlisty.carlisty.utils.Constants
import com.carlisty.carlisty.webservices.BaseResponse
import com.carlisty.carlisty.webservices.dealers.DealerResponse
import com.carlisty.carlisty.webservices.dealers.DealersAPI
import com.carlisty.carlisty.webservices.vehicles.ListVehiclesRequest
import com.carlisty.carlisty.webservices.vehicles.VehicleResponse
import com.carlisty.carlisty.webservices.vehicles.VehiclesAPI
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Raphzz on 12/04/2018.
 */

class VehiclesInteractor : BaseInteractor() {

    fun getVehicles(listener: VehiclesListener, request: ListVehiclesRequest) {
        val api = createAPI(Constants().DOMAIN_URL, VehiclesAPI::class.java) as VehiclesAPI

        val call = api.getVehicles(request)

        call.enqueue(object : Callback<BaseResponse> {
            override fun onResponse(call: Call<BaseResponse>, response: Response<BaseResponse>) {
                if (!response.isSuccessful && response.code() != 200) {
                    listener.onVehiclesError()
                    return
                }

                val baseResponse = response.body()
                val vehicleResponse = VehicleResponse()

                for(content in baseResponse.content!!) {
                    val gson = Gson()
                    val vehicle = gson.fromJson(gson.toJson(content), Vehicle::class.java)
                    vehicleResponse.vehicles.add(vehicle)
                }

                listener.onVehiclesLoaded(vehicleResponse)
            }

            override fun onFailure(call: Call<BaseResponse>, t: Throwable) {
                listener.onVehiclesError()
            }
        })
    }
}