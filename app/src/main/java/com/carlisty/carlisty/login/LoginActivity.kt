package com.carlisty.carlisty.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import br.com.simplepass.loading_button_lib.customViews.CircularProgressButton
import com.carlisty.carlisty.R
import com.carlisty.carlisty.base.BaseNavigationActivity
import com.carlisty.carlisty.dealers.DealersActivity
import com.carlisty.carlisty.models.User
import com.carlisty.carlisty.vehicles.VehiclesActivity
import com.carlisty.carlisty.webservices.BaseError
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : BaseNavigationActivity(), LoginView {

    private val presenter = LoginPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setTitle(this, getString(R.string.login_title))

        setupUI()
    }

    private fun setupUI() {
        login_activity_login_button.setOnClickListener {
            presenter.validateLogin("mike@email.com", "rapha123")
            //presenter.validateLogin(login_email.text.toString(), login_password.text.toString())
        }
    }

    override fun showLoading() {
        login_activity_login_button.startAnimation()
    }

    override fun hideLoading() {
        stopProgress()
    }

    override fun onLoginSuccess(user: User) {
        login_activity_login_button.revertAnimation()
        this.startActivity(Intent(this, DealersActivity::class.java))
    }

    override fun onLoginError(error: BaseError) {
        login_activity_login_button.revertAnimation()
        Toast.makeText(this, error.msg, Toast.LENGTH_LONG).show()
    }

    override fun invalidEmail() {
        this.login_email_layout.isErrorEnabled = true
        this.login_email_layout.error = getString(R.string.login_email_error)
    }

    override fun invalidPassword() {
        this.login_password_layout.isErrorEnabled = true
        this.login_password_layout.error = getString(R.string.login_password_error)
    }
}
