package com.carlisty.carlisty.login

import com.carlisty.carlisty.base.BaseView
import com.carlisty.carlisty.models.User
import com.carlisty.carlisty.webservices.BaseError

interface LoginView: BaseView {

    fun onLoginSuccess(user: User)
    fun onLoginError(error: BaseError)
    fun invalidEmail()
    fun invalidPassword()
}