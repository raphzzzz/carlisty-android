package com.carlisty.carlisty.login

import com.carlisty.carlisty.Globals
import com.carlisty.carlisty.models.User
import com.carlisty.carlisty.utils.StringUtils
import com.carlisty.carlisty.webservices.BaseError
import com.carlisty.carlisty.webservices.login.LoginRequest

class LoginPresenter(private val view: LoginView) : LoginListener {

    fun validateLogin(email: String, password: String) {

        if (!StringUtils.isEmailValid(email)) {
            view.invalidEmail()
            return
        }

        if (password.length < 3) {
            view.invalidPassword()
            return
        }

        view.showLoading()

        val loginRequest = LoginRequest(email = email, pass = password)

        val interactor = LoginInteractor()
        interactor.login(this, loginRequest)
    }

    override fun onLoginSuccess(user: User) {
        if (user.token == null) {
            view.onLoginError(BaseError())
            return
        }

        Globals.currentUser = user

        view.onLoginSuccess(user)
    }

    override fun onLoginError(error: BaseError) {
        view.onLoginError(error)
    }
}