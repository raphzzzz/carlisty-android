package com.carlisty.carlisty.login

import com.carlisty.carlisty.models.User
import com.carlisty.carlisty.webservices.BaseError

interface LoginListener {
    fun onLoginSuccess(user: User)
    fun onLoginError(error: BaseError)
}