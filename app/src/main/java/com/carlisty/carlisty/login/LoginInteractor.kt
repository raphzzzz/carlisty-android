package com.carlisty.carlisty.login

import com.carlisty.carlisty.base.BaseInteractor
import com.carlisty.carlisty.models.User
import com.carlisty.carlisty.utils.Constants
import com.carlisty.carlisty.webservices.BaseEntityResponse
import com.carlisty.carlisty.webservices.BaseError
import com.carlisty.carlisty.webservices.login.LoginAPI
import com.carlisty.carlisty.webservices.login.LoginRequest
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Raphzz on 03/05/2018.
 */

class LoginInteractor : BaseInteractor() {

    fun login(listener: LoginListener, loginRequest: LoginRequest) {
        val api = createAPI(Constants().DOMAIN_URL, LoginAPI::class.java) as LoginAPI

        val call = api.login(loginRequest)

        call.enqueue(object : Callback<BaseEntityResponse> {
            override fun onResponse(call: Call<BaseEntityResponse>, response: Response<BaseEntityResponse>) {

                val baseResponse = response.body()

                val gson = Gson()

                if (!response.isSuccessful && response.code() != 200) {
                    var error = getError(response)

                    if (error == null) {
                        error = BaseError()
                    }

                    listener.onLoginError(error)
                    return
                }

                val user = gson.fromJson(gson.toJson(baseResponse.content), User::class.java)

                listener.onLoginSuccess(user)
            }

            override fun onFailure(call: Call<BaseEntityResponse>, t: Throwable) {
                val error = BaseError()
                listener.onLoginError(error)
            }
        })
    }
}