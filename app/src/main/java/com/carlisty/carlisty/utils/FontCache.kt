package com.carlisty.carlisty.utils

/**
 * Created by Raphzz on 11/04/2018.
 */
import android.content.Context
import android.graphics.Typeface
import java.util.*

/**
 * Created by raphaelpedrinivelasqua on 18/05/17.
 */

object FontCache {
    private val fontCache = Hashtable<String, Typeface>()

    operator fun get(name: String, context: Context): Typeface? {
        var tf: Typeface? = fontCache[name]
        if (tf == null) {
            try {
                tf = Typeface.createFromAsset(context.assets, name)
            } catch (e: Exception) {
                return null
            }

            fontCache.put(name, tf)
        }
        return tf
    }
}