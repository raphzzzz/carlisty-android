package com.carlisty.carlisty.utils

/**
 * Created by Raphzz on 12/04/2018.
 */

class Constants {

    open val SIRV_MEDIA_URL = "https://media.carlisty.com/%s/%s/Walkaround.mp4"

    open val SIRV_360_SPIN_POSFIX = "360.spin"

    open val SIRV_DOMAIN_URL = "https://carlisty.sirv.com/%s/%s/"

    open val SIRV_DOMAIN_IMAGE_URL = "https://carlisty.sirv.com/%s/%s/%s.png?scale.width=500"

    open val DOMAIN_URL = "https://carlisty.herokuapp.com"
}