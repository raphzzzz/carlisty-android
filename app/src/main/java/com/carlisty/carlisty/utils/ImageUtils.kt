package com.carlisty.carlisty.utils

import android.content.ContentResolver
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.net.Uri
import java.io.FileNotFoundException
import android.os.AsyncTask
import android.os.Environment
import android.view.View
import android.widget.ImageView
import java.io.File


/**
 * Created by Raphzz on 13/04/2018.
 */
class ImageUtils {

    fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round(height.toFloat() / reqHeight.toFloat())
            } else {
                inSampleSize = Math.round(width.toFloat() / reqWidth.toFloat())
            }
        }
        return inSampleSize
    }

    fun decodeSampledBitmapFromUri(cr: ContentResolver, uri: Uri,
                                   reqWidth: Int, reqHeight: Int): Bitmap? {

        try {

            // First decode with inJustDecodeBounds=true to check dimensions
            val options = BitmapFactory.Options()
            options.inJustDecodeBounds = true

            BitmapFactory.decodeStream(cr.openInputStream(uri), null, options)

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false
            return BitmapFactory.decodeStream(cr.openInputStream(uri), null, options)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        return null
    }

    fun configBitmapFactory(path: String, widht: Int, height: Int): Bitmap {

        val options = BitmapFactory.Options()
        options.inPreferredConfig = Bitmap.Config.RGB_565
        options.inScaled = false
        options.inJustDecodeBounds = true


        BitmapFactory.decodeFile(path, options)
        options.inSampleSize = calculateInSampleSize(options, widht, height)
        options.inJustDecodeBounds = false

        return bitmapRotation(path, options)!!
    }

    fun saveBitmapInStorage(b: Bitmap) {

//        val mediaStorageDir = File(
//                Environment.getExternalStoragePublicDirectory(
//                        Environment.DIRECTORY_PICTURES), Constants.APP_NAME)
//
//        // Create the storage directory if it does not exist
//        if (!mediaStorageDir.exists()) {
//            if (!mediaStorageDir.mkdirs()) {
//                return
//            }
//        }
//
//        if(Globals.currentUser == null) {
//            return
//        }
//
//        val userDocument = Globals.currentUser!!.cpf
//
//        val file = File(mediaStorageDir.path + File.separator +
//                "profile_" + userDocument + ".jpg")
//
//        if (file.exists()) file.delete()
//        try {
//            val out = FileOutputStream(file)
//            b.compress(Bitmap.CompressFormat.JPEG, 90, out)
//            out.flush()
//            out.close()
//
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
    }

    fun getBitmapFromUri(context: Context, uri: Uri, width: Int, height: Int): Bitmap {
        var b: Bitmap? = configBitmapFactory(uri.path, width, height)

        val path = ""
        if (b == null) {
            try {
                b = decodeSampledBitmapFromUri(
                        context.contentResolver,
                        uri, width, height)

                //b = MediaStore.Images.Media.getBitmap(
                //        NewRefundActivity.this.getContentResolver(), map.getKey());
            } catch (e: Exception) {
                e.printStackTrace()
            }

        }

        saveBitmapInStorage(b!!)

        return b!!
    }

    fun bitmapRotation(caminhoFoto: String, options: BitmapFactory.Options): Bitmap? {

        try {
            val bitmap = BitmapFactory.decodeFile(caminhoFoto, options)

            val exif = ExifInterface(caminhoFoto)
            val exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL)

            var rotate = 0

            when (exifOrientation) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90

                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180

                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
            }

            //		     if (rotate != 0) {
            val w = bitmap.width
            val h = bitmap.height

            // Setting pre rotate
            val mtx = Matrix()
            mtx.preRotate(rotate.toFloat())

            // Rotating Bitmap & convert to ARGB_8888, required by tess
            return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false)

        } catch (e: Exception) {
            return null
        }
    }
}