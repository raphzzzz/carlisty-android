package com.carlisty.carlisty.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Raphzz on 12/04/2018.
 */

class Vehicle : Serializable {

    @SerializedName("_id")
    var id: String? = null
        internal set

    @SerializedName("createdAt")
    var createdAt: String? = null
        internal set

    @SerializedName("updatedAt")
    var updatedAt: String? = null
        internal set

    @SerializedName("dealerID")
    var dealerID: String? = null
        internal set

    @SerializedName("vin")
    var vin: String? = null
        internal set

    @SerializedName("stocknumber")
    var stocknumber: String? = null
        internal set

    @SerializedName("year")
    var year: String? = null
        internal set

    @SerializedName("make")
    var make: String? = null
        internal set

    @SerializedName("model")
    var model: String? = null
        internal set

    @SerializedName("trim")
    var trim: String? = null
        internal set

    @SerializedName("interiorColor")
    var interiorColor: String? = null
        internal set

    @SerializedName("exteriorColor")
    var exteriorColor: String? = null
        internal set

    @SerializedName("mileage")
    var mileage: String? = null
        internal set

    @SerializedName("listprice")
    var listprice: String? = null
        internal set

    @SerializedName("views")
    var views: Int? = null
        internal set

    @SerializedName("sirvId")
    var sirvId: String? = null
        internal set
}