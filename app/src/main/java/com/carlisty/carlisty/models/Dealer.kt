package com.carlisty.carlisty.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Raphzz on 12/04/2018.
 */

class Dealer : Serializable {

    @SerializedName("_id")
    var id: String? = null
        internal set

    @SerializedName("name")
    var name: String? = null
        internal set

    @SerializedName("address")
    var address: String? = null
        internal set

    @SerializedName("profileImage")
    var profileImage: String? = null
        internal set

    @SerializedName("createdAt")
    var createdAt: String? = null
        internal set

    @SerializedName("updatedAt")
    var updatedAt: String? = null
        internal set
}