package com.carlisty.carlisty.models

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Raphzz on 12/04/2018.
 */

class User : Serializable {

    @SerializedName("_id")
    var id: String? = null
        internal set

    @SerializedName("name")
    var name: String? = null
        internal set

    @SerializedName("email")
    var email: String? = null
        internal set

    @SerializedName("token")
    var token: String? = null
        internal set
}